FROM caddy:2-builder-alpine AS builder

RUN xcaddy build \
    --with github.com/caddy-dns/cloudflare \
    --with github.com/caddy-dns/route53 \
    --with github.com/caddy-dns/digitalocean \
    --with github.com/caddy-dns/azure \
    --with github.com/caddy-dns/gandi \
    --with github.com/caddy-dns/googleclouddns \
    --with github.com/caddy-dns/godaddy \
    --with github.com/caddy-dns/namecheap \
    --with github.com/mholt/caddy-l4

FROM caddy:2-alpine

COPY --from=builder /usr/bin/caddy /usr/bin/caddy
